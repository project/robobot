<?php

namespace Drupal\robobot_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Robobot data sync item annotation object.
 *
 * @see \Drupal\robobot_sync\Plugin\RobobotDataSyncManager
 * @see plugin_api
 *
 * @Annotation
 */
class RobobotDataSync extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
