<?php

namespace Drupal\robobot_sync\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\robobot_sync\RobobotSync;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Use the queue to process Robobot items.
 */
class QueueSync extends FormBase {

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The database object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The CronInterface object.
   *
   * @var \Drupal\Core\CronInterface
   */
  protected $cron;

  /**
   * What is the name of the queue.
   *
   * @var string
   */
  protected $queueName;

  /**
   * Robobot syncer.
   *
   * @var RobobotSync
   */
  protected $robobotSync;

  /**
   * Constructor.
   *
   * @param QueueFactory $queue_factory
   *   Queue factory service to get new/existing queues for use.
   * @param Connection $database
   *   The cron service.
   * @param RobobotSync $robobotSync
   *   The Robobot syncer.
   */
  public function __construct(QueueFactory $queue_factory, Connection $database, RobobotSync $robobotSync) {
    $this->queueFactory = $queue_factory;
    $this->robobotSync = $robobotSync;
    $this->database = $database;
    $this->queueName = 'robobot_sync_queue';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static($container->get('queue'), $container->get('database'), $container->get('robobot_sync.robobot_sync'));
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'robobot_sync_queue_sync';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['sync_robobot'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync Robobot'),
      '#submit' => [[$this, 'submitSync']],
    ];

    $form['fill_queue'] = [
      '#type' => 'submit',
      '#submit' => [[$this, 'fillQueue']],
      '#value' => $this->t('Fill queue'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Submit function for the sync button.
   *
   * @param array $form
   *   Form definition array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function submitSync(array &$form, FormStateInterface $form_state) {
    $types = [
      'form_label',
      'form',
      'entry',
    ];
    foreach ($types as $type) {
      $this->robobotSync->sync($type);
    }
  }

  /**
   * Submit function for the insert-into-queue button.
   *
   * @param array $form
   *   Form definition array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function fillQueue(array &$form, FormStateInterface $form_state) {
    $queue = $this->queueFactory->get($this->queueName);
    $queue->createQueue();

    // Retrieve all the unprocessed items from the database.
    // The ordering is to make sure the form_label exists before the form
    // and the form exists before the entry.
    $unprocessedItems = $this->database->query("SELECT * FROM {robobot_data} WHERE sync_status = :sync_status ORDER BY FIELD (type, 'entry', 'form', 'form_label') DESC", [
      ':sync_status' => 0,
    ]);
    foreach ($unprocessedItems as $unprocessedItem) {
      $queue->createItem($unprocessedItem);
    }
  }

}
