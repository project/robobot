<?php

namespace Drupal\robobot_sync\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * A queue worker.
 *
 * @QueueWorker(
 *   id = "robobot_sync_queue",
 *   title = @Translation("Work on processing the synced items."),
 *   cron = {"time" = 60}
 * )
 */
class SyncWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $dataSync = \Drupal::service('robobot_sync.robobot_data_sync');
    $dataSync->sync($data);
  }

}
