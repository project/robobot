<?php

namespace Drupal\robobot_sync\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Robobot data sync plugins.
 */
abstract class RobobotDataSyncBase extends PluginBase implements RobobotDataSyncInterface {

  /**
   * {@inheritdoc}
   */
  public function sync($dataType, $robobotData) {
  }

  /**
   * {@inheritdoc}
   */
  public function map($mapData) {
    $dataSync = \Drupal::service('robobot_sync.robobot_data_sync');
    $dataSync->map($mapData);
  }

  /**
   * {@inheritdoc}
   */
  public function getMappedEntities($dataType, $externalId) {
    $dataSync = \Drupal::service('robobot_sync.robobot_data_sync');
    return $dataSync->getMappedEntities($dataType, $externalId);
  }

}
