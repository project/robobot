<?php

namespace Drupal\robobot_sync\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Robobot data sync plugins.
 */
interface RobobotDataSyncInterface extends PluginInspectionInterface {

  /**
   * Whenever something happens with your Robobot data.
   */
  public function sync($dataType, $robobotData);

  /**
   * Map internal data to external data.
   */
  public function map($mapData);

  /**
   * Get all the mapped entities.
   *
   * @param $dataType
   * @param $robobotData
   *
   * @return mixed
   */
  public function getMappedEntities($dataType, $robobotData);

}
