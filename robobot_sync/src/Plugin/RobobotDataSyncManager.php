<?php

namespace Drupal\robobot_sync\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Robobot data sync plugin manager.
 */
class RobobotDataSyncManager extends DefaultPluginManager {


  /**
   * Constructs a new RobobotDataSyncManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/RobobotDataSync', $namespaces, $module_handler, 'Drupal\robobot_sync\Plugin\RobobotDataSyncInterface', 'Drupal\robobot_sync\Annotation\RobobotDataSync');

    $this->alterInfo('robobot_sync_robobot_data_sync_info');
    $this->setCacheBackend($cache_backend, 'robobot_sync_robobot_data_sync_plugins');
  }

}
