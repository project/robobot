<?php

namespace Drupal\robobot_sync;

/**
 * Class RobobotDataSync.
 */
class RobobotDataSync {

  /**
   * Constructs a new RobobotDataSync object.
   */
  public function __construct() {

  }

  public function sync($data) {
    // Let's call the plugin manager here.
    // We don't implement an event subscriber for this because plugins have
    // more options later for annotation mapping.
    $pluginManager = \Drupal::service('plugin.manager.robobot_data_sync');
    $plugin_definitions = $pluginManager->getDefinitions();
    foreach ($plugin_definitions as $plugin_definition) {
      $pluginManager->createInstance($plugin_definition['id'])->sync($data->type, $data);
    }

    // Now mark the item as processed.
    /** @var \Drupal\robobot_sync\RobobotSync $robobotSync */
    $robobotSync = \Drupal::service('robobot_sync.robobot_sync');
    $robobotSync->markItemProcessed($data->type, $data->did);
  }

  /**
   * {@inheritdoc}
   */
  public function map($mapData) {
    $database = \Drupal::service('database');
    $database->insert('robobot_mapping')
      ->fields($mapData)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getMappedEntities($dataType, $externalId) {
    $database = \Drupal::service('database');

    // Retrieve all the mapped entities from database.
    return $database->query("SELECT * FROM {robobot_mapping} WHERE external_type = :external_type AND external_id = :external_id", [
      ':external_type' => $dataType,
      ':external_id' => $externalId,
    ])->fetchAllAssoc('id');
  }

  /**
   * {@inheritdoc}
   */
  public function getMappedData($internalType, $internalId) {
    $database = \Drupal::service('database');

    // Retrieve all the mapped entities from database.
    return $database->query("SELECT * FROM {robobot_mapping} WHERE internal_type = :internal_type AND internal_id = :internal_id", [
      ':internal_type' => $internalType,
      ':internal_id' => $internalId,
    ])->fetchAllAssoc('id');
  }

  /**
   * {@inheritdoc}
   */
  public function getSyncedData($externalType, $externalId) {
    $database = \Drupal::service('database');

    // Retrieve all the mapped entities from database.
    return $database->query("SELECT * FROM {robobot_data} WHERE type = :type AND did = :did", [
      ':type' => $externalType,
      ':did' => $externalId,
    ])->fetchAssoc();
  }

}
