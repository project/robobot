<?php
/**
 * RobobotSync
 */

namespace Drupal\robobot_sync;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\robobot\RobobotClient;

class RobobotSync {

  const STATUS_UNPROCESSED = 0;
  const STATUS_PROCESSED = 10;

  /**
   * @var \Drupal\robobot\RobobotClient
   */
  protected $robobotClient;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var array
   */
  protected $uniqueIds;

  /**
   * RobobotSync constructor.
   *
   * @param RobobotClient $robobot_client
   * @param ConfigFactory $config_factory
   * @param Connection $database
   * @param TimeInterface $time
   */
  public function __construct(RobobotClient $robobot_client, ConfigFactory $config_factory, Connection $database, TimeInterface $time) {
    $this->robobotClient = $robobot_client;
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->time = $time;
    $this->uniqueIds = [];
  }

  // Todo Move this to batch API.
  public function sync($type) {
    $uri = $this->robobotClient->getEndpointFromType($type);
    if (!$uri) {
      return FALSE;
    }

    // Make request.
    $body = $this->makeRequest($uri);
    $this->syncItems($body['hydra:member'], $type);

    // See if we need to make more requests.
    while (isset($body['hydra:view']['hydra:next'])) {
      $body = $this->makeRequest($body['hydra:view']['hydra:next']);
      $this->syncItems($body['hydra:member'], $type);
    }

    // Now change the status of all the items which are not existent anymore.
    $query = $this->database->select('robobot_data', 'rd');
    if (!empty($this->uniqueIds)) {
      $query->condition('rd.did', $this->uniqueIds, 'NOT IN');
    }
    $query->condition('rd.type', $type, '=');
    $query->condition('rd.removed', 1, '!=');
    $query->fields('rd');
    $result = $query->execute();
    foreach ($result as $obsoleteItem) {
      $obsoleteItem = json_decode(json_encode($obsoleteItem), true);
      $obsoleteItem['removed'] = 1;
      $obsoleteItem['sync_status'] = $this::STATUS_UNPROCESSED;
      $this->saveItem($obsoleteItem);
    }

    return TRUE;
  }

  private function makeRequest($uri) {
    $response = $this->robobotClient->get($uri);
    return Json::decode($response->getBody());
  }

  private function syncItems($items, $type) {
    foreach ($items as $data) {
      $this->uniqueIds[$data['id']] = $data['id'];
      // Fill the robobotData array.
      $robobotData = [];
      $robobotData['did'] = $data['id'];
      $robobotData['type'] = $type;
      $robobotData['iri'] = $this->robobotClient->getEndpointFromType($type) . '/' . $data['id'];
      $robobotData['data'] = serialize($data);
      $robobotData['sync_date'] = $this->time->getRequestTime();
      $robobotData['sync_status'] = $this::STATUS_UNPROCESSED;
      $this->saveItem($robobotData);
    }
  }

  private function saveItem($robobotData) {
    $this->database->merge('robobot_data')
      ->keys([
          'did' => $robobotData['did'],
          'type' => $robobotData['type']
        ])
      ->fields($robobotData)
      ->execute();
  }

  /**
   * Mark the item as processed.
   *
   * @param $type
   * @param $did
   */
  public function markItemProcessed($type, $did) {
    $this->database->update('robobot_data')
      ->fields(['sync_status' => $this::STATUS_PROCESSED])
      ->condition('type', $type)
      ->condition('did', $did)
      ->execute();
  }

}
