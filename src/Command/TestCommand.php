<?php

namespace Drupal\robobot\Command;

use Drupal\Component\Serialization\Json;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;

/**
 * Class TestCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="robobot",
 *     extensionType="module"
 * )
 */
class TestCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('robobot:test')
      ->setDescription($this->trans('Test Robobot connection'));
  }

  /**
   * {@inheritdoc}
   */
  protected function initialize(InputInterface $input, OutputInterface $output) {
    parent::initialize($input, $output);
    $this->getIo()->info('initialize');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    /** @var \Drupal\robobot\RobobotClient $robobotClient */
    $robobotClient = $this->container->get('robobot.robobot_client');
    $response = $robobotClient->get('/api/me');
    print_r(Json::decode($response->getBody()));
  }

}
