<?php
/**
 * RobobotClient
 */

namespace Drupal\robobot;

use Drupal\Component\Serialization\Json;

class RobobotClient {

  /**
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * RobobotClient constructor.
   *
   * @param $http_client_factory \Drupal\Core\Http\ClientFactory
   * @param $config_factory \Drupal\Core\Config\ConfigFactory
   */
  public function __construct($http_client_factory, $config_factory) {
    $options = [
      'base_uri' => 'https://api.robobot.app/',
      'headers' => [
        'accept' => 'application/ld+json',
        'Content-Type' => 'application/ld+json',
      ],
    ];
    // Don't use this on production.
    if ($options['base_uri'] === 'https://h2-proxy:8443/') {
      $options['verify'] = FALSE;
    }

    $this->client = $http_client_factory->fromOptions($options);
    $email = $config_factory->get('robobot.settings')->get('email');
    $password = $config_factory->get('robobot.settings')->get('password');

    // Set the bearer token in the client.
    if ($email && $password && $token = $this->getToken($email, $password)) {
      $options['headers']['Authorization'] = 'Bearer ' . $token;
      $this->client = $http_client_factory->fromOptions($options);
    }
  }

  /**
   * Get the token.
   *
   * @param string $clientCode
   * @param string $secretCode
   * @param string $method
   *
   * @return array
   */
  private function getToken(string $clientCode, string $secretCode, string $method = 'user_credentials') {
    if ($method !== 'user_credentials') {
      throw new \Exception('Method not supported yet.');
    }
    if ($method === 'user_credentials') {
      $response = $this->client->post('api/token', ['body' => json_encode([
        'email' => $clientCode,
        'password' => $secretCode,
      ])]);
    }

    $data = Json::decode($response->getBody());
    if (!$data['token']) {
      throw new \Exception('Could not get the token.');
    }
    return $data['token'];
  }

  /**
   * Make a get request.
   *
   * @param string $uri
   * @param array $options
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function get(string $uri, array $options = []) {
    return $this->client->get($uri, $options);
  }

  /**
   * Make a post request.
   *
   * @param string $uri
   * @param array $options
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function post(string $uri, array $options = []) {
    return $this->client->post($uri, $options);
  }

  /**
   * Returns an endpoint for a given entity type.
   *
   * @param $type
   *
   * @return string|null
   */
  public function getEndpointFromType($type) {
    // Yes this can be more efficient but also need to validate on type for now.
    switch ($type) {
      case 'entry':
        return '/api/entries';
        break;
      case 'form_element_tag':
        return '/api/form_element_tags';
        break;
      case 'form_element_type':
        return '/api/form_element_types';
        break;
      case 'form_element':
        return '/api/form_elements';
        break;
      case 'form_label':
        return '/api/form_labels';
        break;
      case 'form':
        return '/api/forms';
        break;
      case 'media_object':
        return '/api/media_objects';
        break;
      case 'organisation':
        return '/api/organisations';
        break;
      case 'report_element_type':
        return '/api/report_element_types';
        break;
      case 'report_element':
        return '/api/report_elements';
        break;
      case 'report':
        return '/api/reports';
        break;
      case 'user':
        return '/api/users';
        break;
      case 'visibility_rule_form_element_tag':
        return '/api/visibility_rule_form_element_tags';
        break;
      case 'visibility_rule_form_element':
        return '/api/visibility_rule_form_elements';
        break;
      case 'visibility_rule_set':
        return '/api/visibility_rule_sets';
        break;
    }
    return NULL;
  }

}
